# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161116203114) do

  create_table "donacions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.decimal  "cantidad",       precision: 10
    t.date     "fecha_donacion"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "donador_id"
    t.index ["donador_id"], name: "index_donacions_on_donador_id", using: :btree
  end

  create_table "donadors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.string   "direccion"
    t.date     "aniversario"
    t.string   "tipo"
    t.bigint   "telefono"
    t.string   "correo"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "RFC"
  end

  create_table "historials", primary_key: ["idHistorial", "idNino"], force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "idHistorial",              null: false, unsigned: true
    t.integer  "idNino",                   null: false, unsigned: true
    t.string   "Diagnostico"
    t.string   "Medicamentos", limit: 800
    t.boolean  "Crisis"
    t.string   "Medico"
    t.string   "Neurologo"
    t.string   "Alergias"
    t.string   "Cirugias"
    t.string   "Pediatra"
    t.string   "Valvulas"
    t.integer  "Fracturas"
    t.string   "Problemas"
    t.string   "Terapias"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "Comida"
    t.string   "Leche"
    t.string   "Otro"
    t.string   "Progreso"
    t.index ["idHistorial", "idNino"], name: "index_historials_on_idHistorial_and_idNino", unique: true, using: :btree
    t.index ["idHistorial"], name: "index_historials_on_idHistorial", using: :btree
    t.index ["idNino"], name: "index_historials_on_idNino", using: :btree
  end

  create_table "lista", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "cantidad"
    t.date     "fecha_asignacion"
    t.integer  "usuario_id"
    t.integer  "producto_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["producto_id"], name: "index_lista_on_producto_id", using: :btree
    t.index ["usuario_id"], name: "index_lista_on_usuario_id", using: :btree
  end

  create_table "ninos", primary_key: "idNino", unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.string   "ApellidoPaterno"
    t.string   "ApellidoMaterno"
    t.date     "FechaNacimiento"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.integer  "Edad"
    t.date     "Fechaingreso"
    t.string   "Sexo",            limit: 45
    t.boolean  "Activo",                     default: true, null: false
    t.integer  "PorcentajeBeca"
    t.integer  "idTerapeuta"
    t.string   "Direccion"
  end

  create_table "padres", primary_key: ["idUsuario", "idNino"], force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "idUsuario", null: false, unsigned: true
    t.integer "idNino",    null: false, unsigned: true
    t.index ["idNino"], name: "index_padres_on_idNino", using: :btree
    t.index ["idUsuario", "idNino"], name: "index_padres_on_idUsuario_and_idNino", unique: true, using: :btree
    t.index ["idUsuario"], name: "index_padres_on_idUsuario", using: :btree
  end

  create_table "pagos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.decimal "cantidad",   precision: 10
    t.date    "fecha_pago"
    t.integer "usuario_id",                unsigned: true
    t.index ["usuario_id"], name: "idUsuario_idx", using: :btree
  end

  create_table "privilegios", primary_key: "idPrivilegio", unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "Nombre",      limit: 100,   null: false
    t.text   "Descripcion", limit: 65535, null: false
  end

  create_table "productos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre",     null: false
    t.boolean  "existencia", null: false
    t.string   "categoria",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "asignado"
  end

  create_table "proyectos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "idProyecto"
    t.string   "titulo"
    t.text     "descripcion",  limit: 65535
    t.date     "fechaInicial"
    t.date     "fechaFinal"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "activo",                     default: 1
  end

  create_table "roles", primary_key: "idRol", unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "Nombre", limit: 100, null: false
  end

  create_table "roles_privilegios", primary_key: ["idRol", "idPrivilegio"], force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "idRol",        null: false, unsigned: true
    t.integer "idPrivilegio", null: false, unsigned: true
    t.index ["idPrivilegio"], name: "index_roles_privilegios_on_idPrivilegio", using: :btree
    t.index ["idRol", "idPrivilegio"], name: "index_roles_privilegios_on_idRol_and_idPrivilegio", unique: true, using: :btree
    t.index ["idRol"], name: "index_roles_privilegios_on_idRol", using: :btree
  end

  create_table "tareas", primary_key: "idtareas", unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "titulo",         limit: 45,             null: false
    t.string  "descripcion",    limit: 45,             null: false
    t.date    "fechadeentrega",                        null: false
    t.integer "completada",                default: 0, null: false
  end

  create_table "tasks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.text     "descripcion", limit: 65535
    t.integer  "proyecto_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["proyecto_id"], name: "index_tasks_on_proyecto_id", using: :btree
  end

  create_table "terapeuta", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "idUsuario"
    t.integer  "idNino"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["idNino"], name: "index_terapeuta_on_idNino", using: :btree
    t.index ["idUsuario", "idNino"], name: "index_terapeuta_on_idUsuario_and_idNino", unique: true, using: :btree
    t.index ["idUsuario"], name: "index_terapeuta_on_idUsuario", using: :btree
  end

  create_table "usuarios", primary_key: "idUsuario", unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "Nombre",                                     null: false
    t.string   "password_digest",                            null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "ApellidoPaterno", limit: 100,                null: false
    t.string   "ApellidoMaterno", limit: 100,                null: false
    t.date     "FechaNacimiento",                            null: false
    t.string   "Correo",          limit: 100,                null: false
    t.bigint   "Telefono",                                   null: false
    t.string   "Sexo",            limit: 45,                 null: false
    t.string   "Foto",            limit: 200
    t.boolean  "Activo",                      default: true, null: false, unsigned: true
    t.integer  "idRol",                                      null: false, unsigned: true
    t.index ["Correo"], name: "index_usuarios_on_Correo", unique: true, using: :btree
    t.index ["idRol"], name: "idRol_idx", using: :btree
  end

  add_foreign_key "donacions", "donadors"
  add_foreign_key "padres", "ninos", column: "idNino", primary_key: "idNino", name: "fk_idNino_padre"
  add_foreign_key "pagos", "usuarios", primary_key: "idUsuario", name: "idUsuario"
  add_foreign_key "roles_privilegios", "privilegios", column: "idPrivilegio", primary_key: "idPrivilegio", name: "idPrivilegio"
  add_foreign_key "roles_privilegios", "roles", column: "idRol", primary_key: "idRol", name: "idRol"
  add_foreign_key "tasks", "proyectos"
  add_foreign_key "usuarios", "roles", column: "idRol", primary_key: "idRol", name: "fk_idRol_usuarios"
end
