# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

=begin
Privilegio.create!(idPrivilegio:  1,
              Nombre:  "Crear",
              Descripcion:  "Permite crear nuevos registros (nivel sistema)")

Privilegio.create!(idPrivilegio:  2,
              Nombre:  "Editar",
              Descripcion:  "Permite editar registros (nivel sistema)")

Privilegio.create!(idPrivilegio:  3,
              Nombre:  "Eliminar",
              Descripcion:  "Permite eliminar registros (nivel sistema)")

Privilegio.create!(idPrivilegio:  4,
              Nombre:  "Registrar Usuario",
              Descripcion:  "")
              
Privilegio.create!(idPrivilegio:  5,
              Nombre:  "Usuarios",
              Descripcion:  "Da acceso a la sección de Usuarios")              
              
Privilegio.create!(idPrivilegio:  6,
              Nombre:  "Roles",
              Descripcion:  "Da acceso a la sección de administración de Roles")              
              
Privilegio.create!(idPrivilegio:  7,
              Nombre:  "Cocina",
              Descripcion:  "Da acceso a la sección de Cocina")
              
Privilegio.create!(idPrivilegio:  8,
              Nombre:  "Donadores",
              Descripcion:  "Da acceso a la sección de Donadores")              
              
Privilegio.create!(idPrivilegio:  9,
              Nombre:  "Pagos",
              Descripcion:  "Da acceso a la sección de Pagos")
            
Privilegio.create!(idPrivilegio:  10,
              Nombre:  "Proyectos",
              Descripcion:  "Da acceso a la sección de Proyectos")
              
Privilegio.create!(idPrivilegio:  11,
              Nombre:  "Niños",
              Descripcion:  "Da acceso a la sección de Niños")
              
Rol.create!(idPrivilegio:  1,
              Nombre:  "Administrador")
          
Role.create!(idRol:  2,
              Nombre:  "Terapeuta")

Role.create!(idRol:  3,
              Nombre:  "Padre")

Roles_privilegio.create!(idRol:  1,
              idPrivilegio:  "1")

Usuario.create!(Nombre:  "Bernardo",
                ApellidoPaterno:  "Gómez",
                ApellidoMaterno:  "Romero",
                FechaNacimiento:  "1995-08-09",
                Telefono:  "4421234567",
                Correo: "ejemplo@canidace.org",
                Sexo: "Masculino",
                password:              "111111",
                password_confirmation: "111111",
                idRol:     "1")
=end

=begin
100.times do |n|
  nombre  = Faker::Name.first_name
  aPaterno = Faker::Name.last_name
  aMaterno = Faker::Name.last_name
  correo = Faker::Internet.email(nombre + "." + aPaterno)
  password = "password"
  sexo = "Masculino"
  rol = 1
  activo = 1
  if (n%2 == 0)
     sexo = "Femenino" 
     rol = 2
  end
  if (n%4 == 0)
     activo = 0
  end
  Usuario.create!(Nombre:  nombre,
              ApellidoPaterno:  aPaterno,
              ApellidoMaterno:  aMaterno,
              FechaNacimiento:  "1990-01-01",
              Telefono:  "4421234567",
              Correo: correo,
              Sexo: sexo,
              password:              password,
              password_confirmation: password,
              idRol:     rol,
              Activo: activo)
end

100.times do |n|
  name  = Faker::Name.first_name
  aPaterno = Faker::Name.last_name
  aMaterno = Faker::Name.last_name
  active = 1
  if (n%4 == 0)
     active = 0
  end
  sexo = "Masculino" 
  if (n%2 == 0)
     sexo = "Femenino" 
  end
  Nino.create!(nombre:  name,
              ApellidoPaterno:  aPaterno,
              ApellidoMaterno:  aMaterno,
              FechaNacimiento:  "1998-01-01",
              Fechaingreso: "2016-10-15",
              Edad:  "18",
              Sexo: sexo,
              Activo: active)
end


100.times do |n|
  name = Faker::Name.name    
  existence = true
  asigned = 'No asignado'
  if(n%2 == 0)
    existence = false    
  end
  if(n%8 == 0)
    category = 'Frutas'
  elsif(n%8 == 1)
    category = 'Verduras'
  elsif(n%8 == 2)
    category = 'Carnes'
  elsif(n%8 == 3)
    category = 'Granos/Semillas'
  elsif(n%8 == 4)
    category = 'Higiene'
  elsif(n%8 == 5)
    category = 'Abarrotes'
  elsif(n%8 == 6)
    category = 'Congelados'
  else
    category = 'Lacteos'
  end
  Producto.create!(nombre:  name,
              categoria:  category,
              existencia:  existence,
              asignado: asigned)
end

=end
ninos = Nino.order(:created_at).take(6)
10.times do |n|
  Diagnostico = Faker::Lorem.sentence(5)
  Medico = "Dr. "+Faker::Name.first_name+Faker::Name.last_name
  Pediatra = "Dr. "+Faker::Name.first_name+Faker::Name.last_name
  Neurologo = "Dr. "+Faker::Name.first_name+Faker::Name.last_name
  crisis = 1
  if (n%4 == 0)
     crisis = 0
  end
  ninos.each { |nino| nino.historials.create!(Diagnostico: Diagnostico,
                                               Medico: Medico,
                                               Pediatra: Pediatra,
                                               Neurologo: Neurologo,
                                               Crisis: crisis) }
end
