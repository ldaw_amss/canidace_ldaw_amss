class AddRfcToDonadors < ActiveRecord::Migration[5.0]
  def change
    add_column :donadors, :RFC, :string
  end
end
