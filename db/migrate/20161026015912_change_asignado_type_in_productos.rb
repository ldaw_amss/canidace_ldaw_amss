class ChangeAsignadoTypeInProductos < ActiveRecord::Migration[5.0]
  def change
    change_column :productos, :asignado, :string
  end
end
