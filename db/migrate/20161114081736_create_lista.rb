class CreateLista < ActiveRecord::Migration[5.0]
  def change
    create_table :lista do |t|
      t.string :cantidad
      t.date :fecha_asignacion
      t.belongs_to :usuario
      t.belongs_to :producto
      t.timestamps
    end
  end
end
