class CreateDonacions < ActiveRecord::Migration[5.0]
  def change
    create_table :donacions do |t|
      t.decimal :cantidad
      t.date :fecha_donacion

      t.timestamps
    end
  end
end
