class CreateTerapeuta < ActiveRecord::Migration[5.0]
  def change
    create_table :terapeuta do |t|
      t.integer :idUsuario
      t.integer :idNino

      t.timestamps
    end
    
    add_index :terapeuta, :idUsuario
    add_index :terapeuta, :idNino
    add_index :terapeuta, [:idUsuario, :idNino], unique: true
    
  end
end
