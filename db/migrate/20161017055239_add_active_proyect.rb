class AddActiveProyect < ActiveRecord::Migration[5.0]
  def change
    add_column :proyectos, :activo, :integer, :default => 1
  end
end
