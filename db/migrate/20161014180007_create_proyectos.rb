class CreateProyectos < ActiveRecord::Migration[5.0]
  def change
    create_table :proyectos do |t|
      t.integer :idProyecto
      t.string :titulo
      t.text :descripcion
      t.date :fechaInicial
      t.date :fechaFinal
      t.integer :activo

      t.timestamps
    end
  end
end
