class CreatePadres < ActiveRecord::Migration[5.0]
  def change
    #create_table :padres do |t|
    #  t.int :idUsuario
    #  t.int :idNino

    #  t.timestamps
    #end
    add_index :padres, :idUsuario
    add_index :padres, :idNino
    add_index :padres, [:idUsuario, :idNino], unique: true
  end
end
