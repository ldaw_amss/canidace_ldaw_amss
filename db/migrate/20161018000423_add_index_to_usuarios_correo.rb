class AddIndexToUsuariosCorreo < ActiveRecord::Migration[5.0]
  def change
    add_index :usuarios, :Correo, unique: true
  end
end
