class CreateNinos < ActiveRecord::Migration[5.0]
  def change
    create_table :ninos do |t|
      t.string :nombre
      t.string :ApellidoPaterno
      t.string :ApellidoMaterno
      t.date :FechaNacimiento

      t.timestamps
    end
  end
end
