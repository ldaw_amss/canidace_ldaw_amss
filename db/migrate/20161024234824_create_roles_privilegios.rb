class CreateRolesPrivilegios < ActiveRecord::Migration[5.0]
  def change
    #create_table :roles_privilegios do |t|
    #  t.integer :idRol
    #  t.integer :idPrivilegio

    #  t.timestamps
    #end
    add_index :roles_privilegios, :idRol
    add_index :roles_privilegios, :idPrivilegio
    add_index :roles_privilegios, [:idRol, :idPrivilegio], unique: true
  end
end
