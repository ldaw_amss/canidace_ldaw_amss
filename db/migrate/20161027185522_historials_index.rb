class HistorialsIndex < ActiveRecord::Migration[5.0]
  def change
    add_index :historials, :idHistorial
    add_index :historials, :idNino
    add_index :historials, [:idHistorial, :idNino], unique: true
  end
end
