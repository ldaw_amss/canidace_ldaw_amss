require 'test_helper'

class RolesControllerTest < ActionDispatch::IntegrationTest

  setup do
    @role = roles(:admin)
    #@usuario = usuarios(:bernardo)
    #@usuario.authenticate('password')
    log_in_as(@usuario)
    #session[:usuario_id] = @usuario.idUsuario
  end

  test "should get index" do
    get roles_url
    assert_response :success
  end

  test "should create role" do
    assert_difference('Role.count') do
      post roles_url, params: { role: {Nombre: "Test"  } }
    end

    assert_redirected_to role_url(Role.last)
  end

  test "should show role" do
    get role_url(@role)
    assert_response :success
    assert_select 'table#privilegios'
  end

  test "should update role" do
    patch role_url(@role), params: { role: { Nombre:"Test2" } }
    assert_redirected_to roles_path
  end

=begin
  test "should destroy role" do
    assert_difference('Role.count', -1) do
      delete role_url(@role)
    end

    assert_redirected_to roles_url
  end
=end

end
