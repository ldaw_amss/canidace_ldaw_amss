require 'test_helper'

class UsuarioPassControllerTest < ActionDispatch::IntegrationTest
  def setup
    @usuario = usuarios(:bernardo)
    log_in_as(@usuario)
  end
  
  test "reestablecer contraseña con exito" do
    patch update_usuario_pass_path(@usuario), params: { usuario: { password:  "foobar",
                                         password_confirmation: "foobar"} }
    assert_not flash.empty?
    assert_redirected_to @usuario
  end
end
