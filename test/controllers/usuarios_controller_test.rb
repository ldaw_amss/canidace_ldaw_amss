require 'test_helper'

class UsuariosControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @usuario = usuarios(:bernardo)
    #@usuario.authenticate('password')
    log_in_as(@usuario)
    #session[:usuario_id] = @usuario.idUsuario
    @otro_usuario = usuarios(:salomon)
  end
  
  test "debe ir a nuevo" do
    #get registrarusuario_path
    #assert_response :success
  end
  
  #Mostrar usuarios (index)
  test "debe obtener index usuarios" do
    @roles = Role.all
    get usuarios_path
    assert_template 'usuarios/index'
    assert_response :success
    assert_select 'ul.pagination'  #Probar paginacion
  end
  
  #Registrar usuario: Invalido
  test "informacion de registro invalida" do
    #get registrarusuario_path
    assert_no_difference 'Usuario.count' do
    post registrarusuario_path, params: { usuario: { Nombre:  "",
                                         ApellidoPaterno: "",
                                         ApellidoMaterno: "",
                                         FechaNacimiento: "",
                                         Telefono: "",
                                         Correo: "",
                                         Sexo: "",
                                         idRol: "",
                                         password:              "foo",
                                         password_confirmation: "bar" } }
    end
    #assert_template 'usuarios/new'
    #assert_select 'div#error_explanation'
    #assert_select 'div.field_with_errors'
  end
  
  #Registrar usuario: Válido
  #Asignar rol a usuario: Válido
  test "informacion de registro valida" do
    get registrarusuario_path
    #assert_difference 'Usuario.count', 1 do
    post registrarusuario_path, params: { usuario: { Nombre:  "a",
                                         ApellidoPaterno: "a",
                                         ApellidoMaterno: "a",
                                         FechaNacimiento: "2010-01-01",
                                         Correo: "123456@ejemplo.com",
                                         Telefono: "44212345",
                                         Sexo: "Masculino",
                                         idRol: "1",
                                         password:              "foobar",
                                         password_confirmation: "foobar" } }
    #end
    #follow_redirect!
    #assert_template 'usuarios/show'
    #assert_not flash.empty?
    #assert is_logged_in?
  end
  
  #Mostrar Usuario
  test "mostrar usuario" do
    get usuario_path(@usuario)
    assert_template "usuarios/show"
    assert_select 'title', "#{@usuario.Nombre} | CANIDACE"
    #assert_select 'h4', text: "Usuario: #{@usuario.Nombre} #{@usuario.ApellidoPaterno} #{@usuario.ApellidoMaterno}"
  end
  
  #Editar usuario: Invalido
  test "edicion no exitosa" do
    get edit_usuario_path(@usuario)
    assert_template 'usuarios/edit'
    patch usuario_path(@usuario), params: { usuario: { Nombre:  "",
                                             ApellidoPaterno: "",
                                             ApellidoMaterno: "",
                                             FechaNacimiento: "",
                                             Telefono: "",
                                             Correo: "foo@bar",
                                             Sexo: "",
                                             idRol: "",
                                             password:              "foo",
                                             password_confirmation: "bar" } }
    follow_redirect!
    assert_template 'usuarios/edit'
  end
  
  #Editar usuario: Válido
  #Editar Rol de Usuario: Válido
  test "edicion exitosa con redireccion" do
    get edit_usuario_path(@usuario)
    assert_template 'usuarios/edit'
    rol = 2
    correo = "654321@ejemplo.com"
    patch usuario_path(@usuario), params: { usuario: { Nombre:  "Bernardo",
                                         ApellidoPaterno: "Gómez",
                                         ApellidoMaterno: "Romero",
                                         FechaNacimiento: "1995-08-09",
                                         Correo: correo,
                                         Telefono: "44212345",
                                         Sexo: "Masculino",
                                         idRol: rol,
                                         Activo: "0"} }
    assert_not flash.empty?
    assert_redirected_to @usuario
    @usuario.reload
    assert_equal rol,  @usuario.idRol
    assert_equal correo, @usuario.Correo
  end
  
  test "activar usuario con exito" do
    #assert @usuario.Activo == 0
    post activarusuario_path(@usuario), params: { id: @usuario.idUsuario, data: 1 }
    @usuario.reload
    #assert @usuario.Activo == 1
  end
  
  #Buscar usuarios (en index), por nombre completo
  test "debe buscar usuarios" do
    #Obtiene index usuarios
    @roles = Role.all
    get usuarios_path
    assert_response :success
    assert_select 'ul.pagination'  #Probar paginacion
    #Busqueda sin resultados
    post usuarios_path, params: { query: "iablsidbfañsdilfb" }
    assert_template 'usuarios/index'
    assert_select 'div#error'
    #Busqueda exitosa
    post usuarios_path, params: { query: "Bernardo Gómez Rom" }
    assert_template 'usuarios/index'
    assert_select 'table#usuarios'
  end
end
