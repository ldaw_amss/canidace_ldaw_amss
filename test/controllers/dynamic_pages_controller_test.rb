require 'test_helper'

class DynamicPagesControllerTest < ActionDispatch::IntegrationTest
  
=begin
  def setup
    @base_title = "CANIDACE"
  end

  test "should get root" do
    get root_path
    assert_response :success
    assert_select "title", "#{@base_title}"
  end

  test "should get Integrantes" do
    get integrantes_path
    get dynamic_pages_Integrantes_url
    assert_response :success
    assert_select "title", "Integrantes  | #{@base_title}"
  end

  test "should get Cocina" do
    get cocina_path
    get dynamic_pages_Cocina_url
    assert_response :success
    assert_select "title", "Cocina  | #{@base_title}"
  end

  test "should get Donadores" do
    get donadores_path
    get dynamic_pages_Donadores_url
    assert_response :success
    assert_select "title", "Donadores  | #{@base_title}"
  end

  test "should get Proyectos" do
    get proyectos_path
    get dynamic_pages_Proyectos_url
    assert_response :success
    assert_select "title", "Proyectos  | #{@base_title}"
  end
=end

end