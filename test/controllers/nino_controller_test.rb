require 'test_helper'

class NinoControllerTest < ActionDispatch::IntegrationTest

=begin
  def setup
    @nino = ninos(:salomon)
  end
  
  test "debe ir a nuevo" do
    get registrarnino_path
    assert_response :success
  end
  
  #Mostrar usuarios (index)
  test "debe obtener index niños" do
    get ninos_path
    assert_template 'nino/index'
    assert_response :success
  end
  
  #Registrar niño: Invalido
  test "informacion de registro invalida" do
    get registrarnino_path
    assert_no_difference 'Nino.count' do
    post registrarnino_path, params: { nino: { nombre:  "",
                                         ApellidoPaterno: "",
                                         ApellidoMaterno: "",
                                         FechaNacimiento: "",
                                         Edad: "",
                                         Fechaingreso: "",
                                         } }
    end
    assert_template 'nino/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end
  
  #Registrar niño: Válido
  test "informacion de registro valida" do
    get registrarnino_path
    assert_difference 'Nino.count', 1 do
    post registrarnino_path, params: { nino: { nombre:  "a",
                                         ApellidoPaterno: "a",
                                         ApellidoMaterno: "a",
                                         FechaNacimiento: "2010-01-01",
                                         Fechaingreso: "2016-10-19",
                                         Edad: "15", } }
    end
    follow_redirect!
    assert_template 'nino/show'
    assert_not flash.empty?
  end
  
  #Mostrar niño
  test "mostrar niño" do
    get nino_path(@nino)
    assert_template "nino/show"
    assert_select 'h3', text: "keyboard_backspace Perfil de: #{@nino.nombre} #{@nino.ApellidoPaterno} #{@nino.ApellidoMaterno}"
  end
  
  #Editar niño: Invalido
  test "edicion no exitosa" do
    get edit_nino_path(@nino)
    assert_template 'nino/edit'
    patch nino_path(@nino), params: { nino: { nombre:  "",
                                         ApellidoPaterno: "",
                                         ApellidoMaterno: "",
                                         FechaNacimiento: "",
                                         Edad: "2145",
                                         Fechaingreso: "", } }
    assert_template 'nino/edit'
  end
  
  #Editar niño: Válido
  #Editar Nombre: Válido
  test "edicion exitosa con redireccion" do
    get edit_nino_path(@nino)
    assert_template 'nino/edit'
    nombrecambio = "Felipe"
    patch nino_path(@nino), params: { nino: { nombre:  nombrecambio,
                                         ApellidoPaterno: "Olivera",
                                         ApellidoMaterno: "Abud",
                                         FechaNacimiento: "1995-04-21",
                                         Fechaingreso: "2016-09-24",
                                         Edad: "21",
                                         Activo: "1"} }
    assert_redirected_to @nino  
    @nino.reload
    assert_equal nombrecambio, @nino.nombre
  end
  
  #Buscar niños (en index), por nombre completo
  test "debe buscar niños" do
    #Obtiene index usuarios
    get ninos_path
    assert_response :success
    #Busqueda sin resultados
    post ninos_path, params: { query: "iablsidbfañsdilfb" }
    assert_template 'nino/index'
    #Busqueda exitosa
    post ninos_path, params: { query: "Salomon Olivera" }
    assert_template 'nino/index'
    assert_select 'table#ninos'
  end
=end

end

