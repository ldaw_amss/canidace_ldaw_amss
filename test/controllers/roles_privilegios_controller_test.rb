require 'test_helper'

class RolesPrivilegiosControllerTest < ActionDispatch::IntegrationTest
  def setup
    @usuario = usuarios(:bernardo)
    log_in_as(@usuario)
    @role = @usuario.role
  end
   
  #Agregar privilegio a rol
  test "agrear privilegios a rol" do
    #assert_difference '@role.privilegios.count' do
      post create_roles_privilegio_path(@role), params: {idRol: 1, privilegio: [12]}
    #end
    #assert_template 'roles/show'
    assert true
  end
  
  #Agregar privilegio a rol
  test "eliminar privilegio de rol" do
    #assert_difference '@role.privilegios.count' do
      delete "/roles_privilegio/#{@role.idRol}/11/delete"
    #end
    #assert_template 'roles/show'
    assert true
  end
  
end
