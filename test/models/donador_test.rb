require 'test_helper'

class DonadorTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @donador = Donador.new(nombre: "Santiago", direccion: "Epigmenio Gonzalez", tipo: "Donador particular", telefono: 44217481212,
                           correo: "santiAago@gmail.com", aniversario: '1992-10-12')
  end
  
  test "donador valido" do
    assert @donador.valid?
  end
  
  test "nombre debe estar presente" do
    @donador.nombre = "     "
    assert_not @donador.valid?
  end
  
  test "direccion del donador debe estar presente" do
    @donador.direccion = "    "
    assert_not @donador.valid?
  end
  
  test "tipo de donador debe estar presente" do
    @donador.tipo = "     "
    assert_not @donador.valid?
  end
  
  test "telefono debe estar presente" do
    @donador.telefono = nil
    assert_not @donador.valid?
  end
  
  test "correo debe estar presente" do
    @donador.correo = "     "
    assert_not @donador.valid?
  end
  
  test "aniversario del donador debe estar presente" do
    @donador.aniversario = "     "
    assert_not @donador.valid?
  end
  
  test "nombre no debe ser muy largo" do
    @donador.nombre = "a" * 61
    assert_not @donador.valid?
  end

  test "correo no debe ser muy largo" do
    @donador.correo = "a" * 71 + "@example.com"
    assert_not @donador.valid?
  end
  
  test "correo validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @donador.correo = valid_address
      assert @donador.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  
  test "correo validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @donador.correo = invalid_address
      assert_not @donador.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
  
  test "correo addresses should be unique" do
    duplicate_user = @donador.dup
    duplicate_user.correo = @donador.correo.upcase
    @donador.save
    assert_not duplicate_user.valid?
  end
  
  test "correo addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @donador.correo = mixed_case_email
    @donador.save
    assert_equal mixed_case_email.downcase, @donador.reload.correo
  end
=begin

  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end
=end
end
