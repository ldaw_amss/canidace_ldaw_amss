require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @usuario = Usuario.new(Nombre:  "Bernardo", ApellidoPaterno: "Gómez", ApellidoMaterno: "Romero", FechaNacimiento: "1995-08-09",
                           Correo: "hola@ejemplo.com", Telefono: "4421234567", Sexo: "Masculino", Foto: "", idRol: "1",
                           password: "foobar", password_confirmation: "foobar")
  end

  test "usuario valido" do
    assert @usuario.valid?
  end
  
  test "nombre debe estar presente" do
    @usuario.Nombre = "     "
    assert_not @usuario.valid?
  end
  
  test "apellidos deben estar presentes" do
    @usuario.ApellidoPaterno = "     "
    @usuario.ApellidoMaterno = "     "
    assert_not @usuario.valid?
  end
  
  test "fecha de nacimiento debe estar presente" do
    @usuario.FechaNacimiento = "     "
    assert_not @usuario.valid?
  end
  
  test "correo debe estar presente" do
    @usuario.Correo = "     "
    assert_not @usuario.valid?
  end
  
  test "sexo debe estar presente" do
    @usuario.Sexo = "     "
    assert_not @usuario.valid?
  end
  
  test "telefono debe estar presente" do
    @usuario.Telefono = "     "
    assert_not @usuario.valid?
  end
  
  test "idRol debe estar presente" do
    @usuario.idRol = "     "
    assert_not @usuario.valid?
  end
  
  test "nombre no debe ser muy largo" do
    @usuario.Nombre = "a" * 51
    assert_not @usuario.valid?
  end

  test "correo no debe ser muy largo" do
    @usuario.Correo = "a" * 244 + "@example.com"
    assert_not @usuario.valid?
  end
  
  test "validacion de correo debe rechazar correos invalidos" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @usuario.Correo = invalid_address
      assert_not @usuario.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
  
  test "correo electronico debe ser unico" do
    duplicate_user = @usuario.dup
    duplicate_user.Correo = @usuario.Correo.upcase
    @usuario.save
    assert_not duplicate_user.valid?
  end
  
  test "password debe estar presente (no en blanco)" do
    @usuario.password = @usuario.password_confirmation = " " * 6
    assert_not @usuario.valid?
  end

  test "password con longitud minima de 6" do
    @usuario.password = @usuario.password_confirmation = "a" * 5
    assert_not @usuario.valid?
  end


  #test "authenticated? should return false for a user with nil digest" do
  #  assert_not @user.authenticated?(:remember, '')
  #end
  
end