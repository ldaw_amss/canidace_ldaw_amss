require 'test_helper'

class ProductoTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  def setup
    @producto = Producto.new(nombre: "Guayaba", categoria: "Frutas", existencia: true, asignado: false)
  end

  test "should be valid" do
    assert @producto.valid?
  end
  
  test "nombre should be present" do
    @producto.nombre = "     "
    assert_not @producto.valid?
  end
  
  test "categoria should be present" do
    @producto.categoria = "     "
    assert_not @producto.valid?
  end
  
  #test "existencia should be present" do
  #  @producto.existencia = nil
  #  assert_not @producto.valid?
 # end
  
  #test "asignado should be present" do
   # @producto.asignado = nil
  #  assert_not @producto.valid?
  #end
  
  test "nombre should not be too long" do
    @producto.nombre = "a" * 51
    assert_not @producto.valid?
  end

end
