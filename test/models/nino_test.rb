require 'test_helper'

class NinoTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
=begin
  def setup
    @nino = Nino.new(nombre: "Niño ejemplo", ApellidoMaterno: "Apellido ejemplo", ApellidoPaterno: "Apellido ejemplo", FechaNacimiento: "2016-01-01", Fechaingreso: "2016-10-21", Edad: "15")
  end

  test "should be valid" do
    assert @nino.valid?
  end
  
  test "nombre should be present" do
    @nino.nombre = "     "
    assert_not @nino.valid?
  end
  
  test "ApellidoPaterno should be present" do
    @nino.ApellidoPaterno = "     "
    assert_not @nino.valid?
  end
  
  test "ApellidoMaterno should be present" do
    @nino.ApellidoMaterno = "     "
    assert_not @nino.valid?
  end
  
  test "FechaNacimiento should be present" do
    @nino.FechaNacimiento = "     "
    assert_not @nino.valid?
  end
  
  test "nombre should not be too long" do
    @nino.nombre = "a" * 50
    assert_not @nino.valid?
  end
  
  test "ApellidoMaterno should not be too long" do
    @nino.ApellidoMaterno = "a" * 50
    assert_not @nino.valid?
  end
  
  test "ApellidoPaterno should not be too long" do
    @nino.ApellidoPaterno = "a" * 46
    assert_not @nino.valid?
  end
  
  test "FechaNacimiento should not be too long" do
    @nino.FechaNacimiento = "a" * 4 + "a" * 2 + "a" * 2
    assert_not @nino.valid?
  end
=end
  
end
