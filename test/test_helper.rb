ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/reporters"
Minitest::Reporters.use!

class ActiveSupport::TestCase
  fixtures :all

  # Returns true if a test user is logged in.
  def is_logged_in?
    !session[:usuario_id].nil?
  end

  # Log in as a particular user.
  def log_in_as(usuario)
    session[:usuario_id] = usuario.idUsuario
  end
  
end

class ActionDispatch::IntegrationTest

  # Log in as a particular user.
  def log_in_as(usuario, password: 'password')
    #post sesiones_path, params: { correo: usuario.Correo,
    post sesiones_path, params: { correo: "bernardo@ejemplo.com",
                                          password: password } 
  end
end