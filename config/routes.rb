Rails.application.routes.draw do
    #get 'donaciones/index'
   
  root 'dynamic_pages#home'
  get '/bienvenido', to: 'dynamic_pages#welcome'
  
  #get '/cocina', to: 'productos#index'
  resources :productos

  get  '/registrarusuario',  to: 'usuarios#new'
  post '/registrarusuario',  to: 'usuarios#create'
  post '/usuarios',  to: 'usuarios#index'
  get '/usuarios',  to: 'usuarios#index'
  post '/activarusuario',  to: 'usuarios#activate'
  post '/activarproducto', to: 'productos#activate'
  resources :usuarios
  resources :usuario_pass
  patch '/reestablecer/:id',  to: 'usuario_pass#update', as: 'update_usuario_pass'
  
  get  '/registrarpadre',  to: 'padres#new'
  post '/registrarpadre',  to: 'padres#create'
  resources :padres
  
  get '/historial', to: 'historials#index'
  post '/historial', to: 'historials#index'
  get  '/registrarhistorial', to: 'historials#new'
  post '/registrarhistorial', to: 'historials#create'
  delete '/historials/:id', to: 'historials#destroy', as: 'destroy_historial'
  patch 'historials/:id/edit', controller: 'historials', action: :update
  resources :historials
  resources :historial
  
  get '/terapeutas', to: 'terapeutas#index'
  post '/terapeutas', to: 'terapeutas#index'
  get '/terapeutas/:idUsuario', to: 'terapeutas#show'
  get '/terapeutas/:idUsuario/new',  to: 'terapeutas#new', as:'new_nino'
  post '/terapeutas/:idUsuario/new',  to: 'terapeutas#create', as:'assign_nino'
  post '/terapeutas/:idUsuario/:idNino', to: 'terapeutas#destroy', as:'destroy_nino'

  resources :sesiones, only:[:new, :create, :destroy]
  get 'login', to: 'sesiones#new', as:'login'
  #post 'login', to: 'sesiones#create', as:'login'
  get 'logout', to: 'sesiones#destroy', as:'logout'
  
  get  '/roles/new',  to: 'roles#new', as:'new_role'
  get  '/roles/:idRol',  to: 'roles#show'
  resources :roles
  get  '/roles_privilegio/:idRol/new',  to: 'roles_privilegios#new', as:'new_roles_privilegio'
  post '/roles_privilegio/:idRol/new',  to: 'roles_privilegios#create', as:'create_roles_privilegio'
  delete '/roles_privilegio/:idRol/:idPrivilegio/delete',  to: 'roles_privilegios#destroy', as:'delete_roles_privilegio'
  resources :roles_privilegio, only:[:new, :create, :destroy]
  
  resources :donadores
  post '/donadores/new', to: 'donadores#new'
  post '/donadores', to: 'donadores#index'
  post '/donadores/:id/edit', to: 'donadores#edit'
  patch '/donadores/:id', to: 'donadores#update'
  
  resources :pagos
    get '/mi_lista_alimentos', to: 'productos#mi_lista_alimentos'
    get '/pagos_hijo', to: 'pagos#pagos_hijo'
    get '/pagos_por_padre/:id', to: 'pagos#pagos_por_padre'
    #get '/pagos/new/:id', to: 'pagos#new'
    get '/pagos_por_padre/new/:id', to: 'pagos_padre#new'
    get '/pagos', to: 'pagos#index'
  
  resources :donaciones
  
  resources :listas
    get '/lista_asignada/:id', to: 'listas#lista_asignada'
    get '/listas/new/:id', to: 'listas#new'
    get 'asignar_productos/new', to: 'listas#asignar_productos'
    patch '/donadores/:id', to: 'donadores#update'
    
  resources :productos do
    member do
      get :desasignado
    end
  end
  post '/productos/new', to: 'productos#new'
  post '/productos', to: 'productos#index'
  post '/productos/:id/edit', to: 'productos#edit'
  #patch '/productos/:id', to: 'productos#update'
=begin
    get "/donadores" => index
    post "/donadores" => create
    delete "/donadores/:id" => delete
    get "/donadores/:id" => create
    get "/donadores/new" => new
    get "/donadores/:id/edit" => edit
    patch "/donadores/:id" => update
    put "/donadores/:id" => update
=end

  get  '/registrarnino',  to: 'nino#new'
  post  '/registrarnino',  to: 'nino#create'
  get '/ninos', to: 'nino#index'
  post '/ninos', to: 'nino#index'
  post '/activarnino',  to: 'nino#activate'
  resources :nino
  
  post '/proyectos', to: 'proyecto#create'
  get '/proyectos', to: 'proyecto#new'
  resources :proyecto
  get '/proyectos', to: 'proyecto#index'
  get '/proyecto/new', to: 'proyecto#new'
  get '/proyecto/edit', to: 'proyecto#edit'
  get '/proyecto', to: 'proyecto#index'
  
end
