module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "CANIDACE"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
  
  def show_errors(object, field_name)
    if object.errors.any?
      if !object.errors.messages[field_name].blank?
        object.errors.messages[field_name].join(", ")
        #in the form, write the p tag for each input field
        #<p class='error'><%= show_errors(@donador, :telefono) %></p>
      end
    end
  end
  
  def tiene_permiso(usuario, idPrivilegio)
    if usuario == nil 
      return false
    else
      @role = Role.find(usuario.idRol)
      @rolePrivilegios = @role.privilegios
      return @rolePrivilegios.any? {|h| h[:idPrivilegio] == idPrivilegio}
    end
  end
  
end