module SesionesHelper
  
  # Logs in the given user.
  def log_in(usuario)
    session[:usuario_id] = usuario.idUsuario
  end
  
  # Remembers a user in a persistent session.
  def remember(usuario)
    usuario.remember
    cookies.permanent.signed[:idUsuario] = usuario.idUsuario
    cookies.permanent[:remember_token] = usuario.remember_token
  end
  
  # Returns true if the given user is the current user.
  def current_user?(usuario)
    usuario == current_user
  end
  
  # Returns the current logged-in user (if any).
=begin
  def current_user
    if (idUsuario = session[:idUsuario])
      @current_user ||= Usuario.find_by(idUsuario: idUsuario)
    elsif (idUsuario = cookies.signed[:idUsuario])
      usuario = Usuario.find_by(id: user_id)
      if usuario && usuario.authenticated?(:remember, cookies[:remember_token])
        log_in usuario
        @current_user = usuario
      end
    end
  end
=end
  def current_user
    return Usuario.where(idUsuario: session[:usuario_id]).first
  end

  # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !current_user.nil?
  end
  
  # Forgets a persistent session.
  def forget(usuario)
    usuario.forget
    cookies.delete(:idUsuario)
    cookies.delete(:remember_token)
  end

  # Logs out the current user.
  def log_out
    forget(current_user)
    session.delete(:idUsuario)
    @current_user = nil
  end
  
  # Redirects to stored location (or to the default).
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Stores the URL trying to be accessed.
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
end