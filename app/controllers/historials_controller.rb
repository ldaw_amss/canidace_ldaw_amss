class HistorialsController < ApplicationController
    before_action :validate_user
    def index
        @nino = Nino.find(params[:format])
        @historials = @nino.historials.paginate(page: params[:page])
    end
    
    def new
        @nino = Nino.find(params[:format])
        if(@nino.historials.any?)
            historialP = @nino.historials.order("created_at").first
            @historial = Historial.new(:Diagnostico => historialP[:Diagnostico], 
                :Medicamentos => historialP[:Medicamentos], :Crisis => historialP[:Crisis], :Medico => historialP[:Medico], 
                :Pediatra => historialP[:Pediatra], :Neurologo => historialP[:Neurologo], :Alergias => historialP[:Alergias], 
                :Cirugias => historialP[:Cirugias], :Valvulas => historialP[:Valvulas], :Problemas => historialP[:Problemas], 
                :Terapias => historialP[:Diagnostico], :Fracturas => historialP[:Diagnostico], 
                :Comida => historialP[:Comida], :Leche => historialP[:Leche], :Otro => historialP[:Otro], :Progreso => historialP[:Progreso])
        else
            @historial = Historial.new()
        end
    end
  
    def create
        @idNino = params[:idNino]
        @nino = Nino.find(params[:historial][:idNino])
        @historial = @nino.historials.new(historial_params)
        if @historial.save 
            flash[:Exito] = "Historial registrado exitosamente!"
            puts "-------------ID Historial------------------"
            @historial = @nino.historials.order("created_at").first
            puts @historial.inspect
            redirect_to "/historials/#{@historial.idHistorial}"
        else
            render 'new'
        end
    end
    
    def show
        @historial = Historial.find_by(idHistorial: params[:id])
    end
    
    def edit
        @historial = Historial.find_by(idHistorial: params[:id])
        @nino = @historial.nino
    end
    
    def update
        @historial = Historial.find_by(idHistorial: params[:id])
        historialP = params[:historial]
        if (Historial.where(idHistorial: params[:id]).limit(1).update_all(:Diagnostico => historialP[:Diagnostico], 
            :Medicamentos => historialP[:Medicamentos], :Crisis => historialP[:Crisis], :Medico => historialP[:Medico], 
            :Pediatra => historialP[:Pediatra], :Neurologo => historialP[:Neurologo], :Alergias => historialP[:Alergias], 
            :Cirugias => historialP[:Cirugias], :Valvulas => historialP[:Valvulas], :Problemas => historialP[:Problemas], 
            :Terapias => historialP[:Diagnostico], :Fracturas => historialP[:Diagnostico], 
            :Comida => historialP[:Comida], :Leche => historialP[:Leche], :Otro => historialP[:Otro], :Progreso => historialP[:Progreso]))
            flash[:Exito] = "Historial editado exitosamente!"
            redirect_to "/historials/#{@historial.idHistorial}"
        else
            render 'edit'
        end
    end
    
    def destroy
        Historial.delete_all(idHistorial: params[:id])
        flash[:Exito] = "Historial borrado correctamente"
        redirect_to :back
    end
    
    private

    def historial_params
      params.require(:historial).permit(:Diagnostico, :Medicamentos, :Crisis, :Medico, :Pediatra, :Neurologo, :Alergias, :Cirugias, 
      :Valvulas, :Problemas, :Terapias, :Fracturas, :Comida, :Leche, :Otro, :Progreso)
    end
end