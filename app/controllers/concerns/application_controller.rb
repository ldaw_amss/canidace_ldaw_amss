class ApplicationController < ActionController::Base
  
  
  before_action :set_locale
 
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
=begin
  def default_url_options(options={})
    { locale: I18n.locale}
  end
=end
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  include SesionesHelper

  private
    def tiene_permiso(usuario, idPrivilegio)
      if usuario == nil 
        return false
      else
        @role = Role.find(usuario.idRol)
        @rolePrivilegios = @role.privilegios
        return @rolePrivilegios.any? {|h| h[:idPrivilegio] == idPrivilegio}
      end
    end
  
    def tiene_permiso_controller(usuario, idPrivilegio)
      if tiene_permiso(usuario, idPrivilegio) == true
        return true
      else
        store_location
        flash[:precaucion] = "Url no encontrado"
        redirect_to root_path
      end
    end

    # Confirms a logged-in user.
    def validate_user
      unless logged_in?
        store_location
        flash[:precaucion] = "Favor de iniciar sesion."
        redirect_to login_url
      end
    end
    
    def current_user
      Usuario.where(idUsuario: session[:usuario_id]).first
    end
    helper_method :current_user
end