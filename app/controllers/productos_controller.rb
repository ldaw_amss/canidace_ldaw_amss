class ProductosController < ApplicationController
  before_action :validate_user
  before_action :set_producto, only: [:show,:edit,:update,:destroy]

  #get /donadores
  def index
=begin 
    if(params[:query] != nil)
      require 'will_paginate/array'
      @productos = Producto.nombre(params[:query]).paginate(page: params[:page])
    else
      @productos = Producto.all.paginate(page: params[:page])
    end
=end
    #@productos = Producto.all
    @productos = Producto.paginate(page: params[:page])
    @listas = Lista.all
    @frutas = Producto.search(params[:search]).where(:categoria => "Frutas").order(:nombre)#.paginate(:page => params[:page], :per_page => 10)
    @verduras = Producto.search(params[:search]).where(:categoria => "Verduras").order(:nombre)#.paginate(:page => params[:page], :per_page => 10)
    @carnes = Producto.search(params[:search]).where(:categoria => "Carnes").order(:nombre)#.paginate(:page => params[:page], :per_page => 10)
    @abarrotes = Producto.search(params[:search]).where(:categoria => "Abarrotes").order(:nombre)#.paginate(:page => params[:page], :per_page => 10)
    @higiene = Producto.search(params[:search]).where(:categoria => "Higiene").order(:nombre)#.paginate(:page => params[:page], :per_page => 10)
    @granos = Producto.search(params[:search]).where(:categoria => "Granos/Semillas").order(:nombre)#.paginate(:page => params[:page], :per_page => 10)
    @congelados = Producto.search(params[:search]).where(:categoria => "Congelados").order(:nombre)#.paginate(:page => params[:page], :per_page => 10)
    @lacteos = Producto.search(params[:search]).where(:categoria => "Lacteos").order(:nombre)#.paginate(:page => params[:page], :per_page => 10)
  end
  
  def new
    @producto = Producto.new
  end
  
  #POST /productos
  def create
    @producto = Producto.new(producto_params)

   
    respond_to do |format|
        if @producto.save
           #flash[:notice] = 'Successfully created project.'
           #redirect_to donadores_path
          format.html { redirect_to productos_path, notice: 'Producto registrado' }
          format.json { render action: 'index', status: :created, location: @producto}
          # added:
          format.js   { render action: 'index', status: :created, location: @producto }
        else
            #render :new
          format.html { render action: 'new' }
          format.json { render json: @producto.errors, status: :unprocessable_entity }
          # added:
          format.js   { render json: @producto.errors, status: :unprocessable_entity }
        end
    end
  end

  #GET /donadores/:id
  def show
  
  end
  
  def edit
    set_producto
    #@producto = Producto.find(params[:id])
  end
  
  def update
    respond_to do |format|
      if set_producto.update_attributes(producto_params_edit)
        flash[:Exito] = "Datos del producto actualizados"
        format.html { redirect_to productos_path}
        format.json { render :index, status: :ok,location: @producto }
      else
        format.html {render :edit}
        format.json {render json: @producto.errors,status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @producto.destroy
    respond_to do | format |
      flash[:Exito] = "Producto eliminado"
      format.html {redirect_to productos_url}
      format.json {head :no_content}
    end
  end
  
  def activate
    set_producto
    #@producto = Producto.find(params[:id])
    if set_producto.update_attribute(:existencia, params[:data])
      #flash[:Exito] = "Usuario #{activo}"
    else
      
    end
     #redirect_to '/usuarios'
  end
  
  def desasignado
    #@productos = Producto.update_all(asignado: "No asignado")
    @productos = Producto.all
    respond_to do |format|
      if Lista.delete_all#@productos.update_all(asignado: "No asignado")
        flash[:Exito] = "Se han quitado las asignaciones"
        format.html { redirect_to productos_path}
        format.json { render :index, status: :ok,location: @producto }
      else
        flash[:Error] = "Error al remover las asignaciones"
        format.html {render :index}
        format.json {render json: @producto.errors,status: :unprocessable_entity }
      end
    end
  end
  
  def mi_lista_alimentos
    @padre = current_user
    @listas = Lista.where(usuario_id: @padre.idUsuario).paginate(:per_page => 20, :page => params[:page])
  end
  
  private
  
  def set_producto
    @producto = Producto.find(params[:id])
  end
  def producto_params
    params.require(:producto).permit(:nombre, :categoria,:existencia,:asignado)
  end
  
  def producto_params_edit
    params.require(:producto).permit(:nombre, :categoria,:asignado)
  end
end
