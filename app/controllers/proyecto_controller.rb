class ProyectoController < ApplicationController
  before_action :validate_user
  def index
    @proyectos = Proyecto.all
  end
  def new
    p "new"
    @proyecto = Proyecto.new
  end
  def create
    p "create"
    @proyecto = Proyecto.new(proyecto_params)    # Not the final implementation!
    if @proyecto.save
      redirect_to "/proyectos"
      # Handle a successful save.
    else
      render 'new'
    end
  end
  def proyecto_params
    params.require(:proyecto).permit(:titulo, :descripcion, :fechaInicial, :fechaFinal)
  end
end
