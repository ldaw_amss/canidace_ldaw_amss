class PagosPadreController < ApplicationController
  
  def new
    @pago = Pago.new
    @padre = Usuario.find(params[:id])
    @ninos = Nino.all
    @padres = Padre.all
  end
=begin 
  def create
    @pago = Pago.new(pago_params)

    respond_to do |format|
        if @pago.save
           #flash[:notice] = 'Successfully created project.'
           #redirect_to donadores_path
          format.html { redirect_to pagos_path, notice: 'Pago registrado' }
          format.json { render action: 'index', status: :created, location: @pago}
          # added:
          format.js   { render action: 'index', status: :created, location: @pago }
        else
            #render :new
          format.html { render action: 'new' }
          format.json { render json: @pago.errors, status: :unprocessable_entity }
          # added:
          format.js   { render json: @pago.errors, status: :unprocessable_entity }
        end
    end
  end
=end 
  def create
    @pago = Pago.new(pago_params)
    if @pago.save
        flash[:success] = "Pago registrado exitosamente!"
        redirect_to "/pagos_por_padre/#{@pago.usuario_id}"#@nino
    else
        render 'new'
    end
  end
  
  private
  
  def pago_params
    params.require(:pago).permit(:cantidad, :fecha_pago,:usuario_id)
  end
end
