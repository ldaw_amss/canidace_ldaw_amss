class PadresController < ApplicationController
  before_action :validate_user
  
  before_action do
    tiene_permiso_controller(current_user, 5)
  end
  
  before_action only: [:new, :create] do
    tiene_permiso_controller(current_user, 1)
  end
  
  before_action only: [:edit] do
    tiene_permiso_controller(current_user, 2)
  end
  
  def edit
     @usuario = Usuario.find(params[:id])
     #render :edit
  end
    
  def show
    @usuario = Usuario.find(params[:id])
    @roles = Role.all
  end

  def new
    @usuario = Usuario.new
    @nino = Nino.find(params[:format])
    @role = Role.find_by(Nombre: "Padre")
  end
  
  def create
    @usuario = Usuario.new(usuario_params);
    @idNino = params[:idNino];
    if @usuario.save 
      @padre = Padre.new(idUsuario: @usuario.id, idNino: @idNino)
      if @padre.save
        flash[:Exito] = "Padre registrado exitosamente!"
        @roles = Role.all
        redirect_to @usuario
      else
        @roles = Role.all
        render "new", :locals => {:@nino => Nino.find(@idNino), :@role => Role.find_by(Nombre: "Padre")}
      end
    else
      @roles = Role.all
      render "new", :locals => {:@nino => Nino.find(@idNino), :@role => Role.find_by(Nombre: "Padre")}
    end
  end

  private
    def usuario_params
      params.require(:usuario).permit(:Nombre, :ApellidoPaterno, :ApellidoMaterno, 
                                      :FechaNacimiento, :Telefono, :Correo, :Sexo, 
                                      :idRol, :password, :password_confirmation)
    end
    
end