class ListasController < ApplicationController
  def index
    @listas = Lista.select('DISTINCT usuario_id').joins(:usuario).merge(Usuario.order(:Nombre))
    @padres = Padre.all
    #@padres = Padre.where(idNino: Nino.where(Activo: true))
    @ninos = Nino.all
    @listass = Lista.all
    @usuarios = Usuario.all
  end
  
  def new
    @lista = Lista.new
    @productos = Producto.where(existencia: false)
    @padre = Usuario.find(params[:id])
  end
  
  def asignar_productos
    @lista = Lista.new
    @productos = Producto.where(existencia: false)
    @padres = Padre.where(idNino: Nino.where(Activo: true))
  end
  
  def create
    @lista = Lista.new(lista_params)
    #@padre = Usuario.where(idUsuario: @lista.usuario_id)
    respond_to do |format|
        if @lista.save
           #flash[:notice] = 'Successfully created project.'
           #redirect_to donadores_path
          format.html { redirect_to "/lista_asignada/#{@lista.usuario_id}", notice: 'Producto asignado' }
          format.json { render action: 'lista_asignada', status: :created, location: @lista}
          # added:
          format.js   { render action: 'lista_asignada', status: :created, location: @lista }
        else
            #render :new
          format.html { render action: 'new' }
          format.json { render json: @lista.errors, status: :unprocessable_entity }
          # added:
          format.js   { render json: @lista.errors, status: :unprocessable_entity }
        end
    end
  end
  
  def edit
    @lista = Lista.find(params[:id])
    @padre = Usuario.find(@lista.usuario_id)
    @padres = Padre.all
    @producto = Producto.find(@lista.producto_id)
  end
  
  def update
    @lista = Lista.find(params[:id])
    respond_to do |format|
      if @lista.update_attributes(lista_params)
        flash[:Exito] = "Asignacion actualizada"
        format.html { redirect_to "/lista_asignada/#{@lista.usuario_id}"}
        format.json { render :index, status: :ok,location: @lista }
      else
        format.html {render :edit}
        format.json {render json: @lista.errors,status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @lista = Lista.find(params[:id])
    @lista.destroy
    respond_to do | format |
      flash[:Exito] = "Asignacion eliminada"
      format.html {redirect_to "/lista_asignada/#{@lista.usuario_id}"}
      format.json {head :no_content}
    end
  end
  
  def lista_asignada
    @listas = Lista.where(usuario_id: params[:id])
    @padre = Usuario.find(params[:id])
  end
  
  private
  
  def set_lista
    @lista = Lista.find(params[:id])
  end
  
  def lista_params
    params.require(:lista).permit(:cantidad, :fecha_asignacion,:usuario_id,:producto_id)
  end
end
