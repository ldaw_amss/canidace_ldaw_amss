class TerapeutasController < ApplicationController
    def index
        if(params[:query] != nil)
          require 'will_paginate/array'
          @usuarios = Usuario.nombre_completo_terapeuta(params[:query]).paginate(page: params[:page])
        else
          @usuarios = Usuario.where(idRol: 2).order(:Nombre).paginate(page: params[:page])
        end
        @roles = Role.all
    end
    
    def show
        @usuario = Usuario.find(params[:idUsuario])
        @nino = Nino.where(idTerapeuta: params[:idUsuario]).paginate(page: params[:page])
    end
    
    def new
        @nino = Nino.where(idTerapeuta: nil, Activo: 1)  
    end
    
    def create
        @idUsuario = params[:idUsuario]
        @idNino = params[:nino]
        if(@idNino.count > 0)
            @idNino.each do |p| 
                if Nino.where(idNino: p).update(idTerapeuta: params[:idUsuario])
                    flash[:Exito] = "Niño(s) añadidos!"
                else
                    flash[:Error] = "Error al intentar guardar a los niños asignados. Intentelo más tarde."
                end
        end
        else
            flash[:Error] = "No se han seleccionado niños"
        end
        redirect_to "/terapeutas/#{@idUsuario}"
    end
    
    def destroy
        @idUsuario = params[:idUsuario]
        if Nino.where(idNino: params[:idNino]).update(idTerapeuta: nil)
            flash[:Exito] = "¡Niño desasignado correctamente!"
        else
            flash[:Error] = "Error al intentar desasignar al niño. Intentelo más tarde"
        end
        redirect_to "/terapeutas/#{@idUsuario}"
    end
end
