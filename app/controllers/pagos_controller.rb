class PagosController < ApplicationController
  before_action :validate_user
  before_action :set_pago, only: [:show,:edit,:update,:destroy]
  
  def index
    #@pagos = Pago.all.paginate(:per_page => 20, :page => params[:page])
    @pagos = Pago.select('DISTINCT usuario_id').joins(:usuario).merge(Usuario.order(:Nombre))
    #@pagos = Pago.uniq{|x| x.usuario_id}.paginate(:per_page => 20, :page => params[:page])
    #@usuarios = Usuario.where(idRol: 3, Activo: true)
    #@pagos = @pag.map{|t| t.usuario_id}.uniq 
    @ninos = Nino.all
    @padres = Padre.all
  end
  
  def new
    @pago = Pago.new
    @padres = Usuario.where(idRol: 3)
    #@padre = Usuario.find(params[:id])
    #@ninos = Nino.all
    #@padres = Padre.all
  end
  
  def create
    @pago = Pago.new(pago_params)
    if @pago.save
        flash[:success] = "Pago registrado exitosamente!"
        redirect_to "/pagos_por_padre/#{@pago.usuario_id}"#@nino
    else
        render 'new'
    end
  end
=begin
  def create
    @pago = Pago.new(pago_params)

    respond_to do |format|
        if @pago.save
           #flash[:notice] = 'Successfully created project.'
           #redirect_to donadores_path
          format.html { redirect_to pagos_path, notice: 'Pago registrado' }
          format.json { render action: 'index', status: :created, location: @pago}
          # added:
          format.js   { render action: 'index', status: :created, location: @pago }
        else
            #render :new
          format.html { render action: 'new' }
          format.json { render json: @pago.errors, status: :unprocessable_entity }
          # added:
          format.js   { render json: @pago.errors, status: :unprocessable_entity }
        end
    end
  end
=end

  def show
    @pago = Pago.find(params[:id])
  end
  
  def edit
    @pago = Pago.find(params[:id])
    @padre = Usuario.find(@pago.usuario_id)
    @ninos = Nino.all
    @padres = Padre.all
  end
  
  def update
    @pago = Pago.find(params[:id])
    respond_to do |format|
      if @pago.update_attributes(pago_params)
        flash[:Exito] = "Datos del pago actualizado"
        format.html { redirect_to "/pagos_por_padre/#{@pago.usuario_id}"}
        format.json { render :index, status: :ok,location: @pago }
      else
        format.html {render :edit}
        format.json {render json: @pago.errors,status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @pago.destroy
    respond_to do | format |
      flash[:Exito] = "Registro de pago eliminado"
      format.html {redirect_to "/pagos_por_padre/#{@pago.usuario_id}"}
      format.json {head :no_content}
    end
  end
  
  def pagos_hijo
    #@usuarios = Usuario.where(idRol: 3, Activo: true)
    @ninos = Nino.all
    @padre = current_user
    @pagos = Pago.where(usuario_id: @padre.idUsuario).paginate(:per_page => 20, :page => params[:page])
    @padres = Padre.all
  end
  
  def pagos_por_padre
    @padre = Usuario.find(params[:id])
    @pagos = Pago.where(usuario_id: @padre.idUsuario).order(fecha_pago: :desc)
    #@usuarios = Usuario.where(idRol: 3, Activo: true)
    @ninos = Nino.all
    @padres = Padre.all
  end
  
  private
  
  def set_pago
    @pago = Pago.find(params[:id])
  end
  
  def pago_params
    params.require(:pago).permit(:cantidad, :fecha_pago,:usuario_id)
  end
end
