class DonacionesController < ApplicationController
  before_action :validate_user
  before_action :set_donacion, only: [:show,:edit,:update,:destroy]
  
  def index
    @donaciones = Donacion.all.paginate(:per_page => 20, :page => params[:page]).order(fecha_donacion: :desc)
    #@donaciones = Donacion.search(params[:search]).paginate(:per_page => 15, :page => params[:page]).order(:donador_id)
  end
  
  def new
    @donacion = Donacion.new
  end
  
  #POST /donadores
  def create
    @donacion = Donacion.new(donacion_params)

   
    respond_to do |format|
        if @donacion.save
           #flash[:notice] = 'Successfully created project.'
           #redirect_to donadores_path
          format.html { redirect_to donaciones_path, notice: 'Donacion registrada' }
          format.json { render action: 'show', status: :created, location: @donacion}
          # added:
          format.js   { render action: 'show', status: :created, location: @donacion }
        else
            #render :new
          format.html { render action: 'new' }
          format.json { render json: @donacion.errors, status: :unprocessable_entity }
          # added:
          format.js   { render json: @donacion.errors, status: :unprocessable_entity }
        end
    end
  end

  #GET /donadores/:id
  def show
  
  end
  
  def edit
    set_donacion
    #@donacion = Donacion.find(params[:id])
  end
  
  def update
    set_donacion
    #@donacion = Donacion.find(params[:id])
    respond_to do |format|
      if set_donacion.update_attributes(donacion_params)
        flash[:Exito] = "Datos de la donación actualizada"
        format.html { redirect_to donaciones_path}
        format.json { render :index, status: :ok,location: @donacion }
      else
        format.html {render :edit}
        format.json {render json: @donacion.errors,status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @donacion.destroy
    respond_to do | format |
      flash[:Exito] = "Donación eliminada"
      format.html {redirect_to donaciones_url}
      format.json {head :no_content}
    end
  end
  
  private
  
  def set_donacion
    @donacion = Donacion.find(params[:id])
  end
  def donacion_params
    params.require(:donacion).permit(:cantidad, :fecha_donacion,:donador_id)
  end
end
