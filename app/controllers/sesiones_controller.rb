class SesionesController < ApplicationController
  def new
  end

  def create
    #puts params[:correo]
    #puts params[:password]
    @usuario = Usuario.find_by(correo: params[:correo])
    #si existe el usuario, si la contrasena es correcta y si esta activo
    if @usuario && @usuario.authenticate(params[:password]) && (@usuario.Activo=true)
      log_in(@usuario)
      flash[:Exito] = 'Bienvenido!'
      redirect_to "/bienvenido"
    #reload login
    else
      flash[:Error] = 'Cuenta o contraseña no existe'
      render :new 
    end
  end

  def destroy
    session[:usuario_id] = nil
    redirect_to root_url
  end
end
