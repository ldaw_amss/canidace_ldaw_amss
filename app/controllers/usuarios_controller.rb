class UsuariosController < ApplicationController
  before_action :validate_user
  
  before_action only: [:show] do
    #tiene_permiso_controller(current_user, 5) or tiene_permiso_controller(current_user, 12)
  end
  
  before_action only: [:new, :create] do
    tiene_permiso_controller(current_user, 1)
  end
  
  before_action only: [:new, :create] do
    tiene_permiso_controller(current_user, 4)
  end
  
  before_action only: [:edit, :update, :activate] do
    tiene_permiso_controller(current_user, 2)
  end
  

  def index
    if(params[:query] != nil)
      require 'will_paginate/array'
      @usuarios = Usuario.nombre_completo(params[:query]).paginate(page: params[:page])
    else
      @usuarios = Usuario.all.order(:Nombre).paginate(page: params[:page])
    end
    @roles = Role.all
  end
  
  def show
    @usuario = Usuario.find(params[:id])
    if(!!@usuario.padre)
      @nino = @usuario.padre.nino
    end  
    @roles = Role.all
  end

  def new
    @usuario = Usuario.new
    @roles = Role.all
  end
  
  def create
    @usuario = Usuario.new(usuario_params)
    if @usuario.save
      flash[:Exito] = "Usuario registrado exitosamente!"
      @usuarios = Usuario.all
      @roles = Role.all
      redirect_to @usuario
    else
      @roles = Role.all
      render 'new'
    end
  end
  
  def edit
    @usuario = Usuario.find(params[:id])
    @roles = Role.all
  end

  def update
    @usuario = Usuario.find(params[:id])
    usuario_params.delete(:password)
    usuario_params.delete(:password_confirmation)
    if @usuario.update_attributes(update_usuario_params)
      flash[:Exito] = "Usuario actualizado"
      redirect_to @usuario
    else
      redirect_to edit_usuario_path
    end
  end
  
  def activate
    @usuario = Usuario.find(params[:id])
    if @usuario.update_attribute(:Activo, params[:data])
      #flash[:Exito] = "Usuario #{activo}"
    else
      
    end
     #redirect_to '/usuarios'
  end

  private

    def usuario_params
      params.require(:usuario).permit(:Nombre, :ApellidoPaterno, :ApellidoMaterno, 
                                      :FechaNacimiento, :Telefono, :Correo, :Sexo, 
                                      :idRol, :password, :password_confirmation)
    end
    
    def update_usuario_params
      params.require(:usuario).permit(:Nombre, :ApellidoPaterno, :ApellidoMaterno, 
                                      :FechaNacimiento, :Telefono, :Correo, :Sexo, 
                                      :idRol, :Activo)
    end
    
end


