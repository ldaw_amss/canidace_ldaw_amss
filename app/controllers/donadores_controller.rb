class DonadoresController < ApplicationController
  before_action :validate_user
  before_action :set_donador, only: [:show,:edit,:update,:destroy]
  #get /donadores
  def index
    #@donadores = Donador.all
    @donadores = Donador.search(params[:search]).paginate(:per_page => 15, :page => params[:page]).order(:nombre)
  end
  
  def new
    @donador = Donador.new
  end
  
  #POST /donadores
  def create
    @donador = Donador.new(donador_params)

   
    respond_to do |format|
        if @donador.save
           #flash[:notice] = 'Successfully created project.'
           #redirect_to donadores_path
          format.html { redirect_to donadores_path, notice: 'Donador registrado' }
          format.json { render action: 'show', status: :created, location: @donador}
          # added:
          format.js   { render action: 'show', status: :created, location: @donador }
        else
            #render :new
          format.html { render action: 'new' }
          format.json { render json: @donador.errors, status: :unprocessable_entity }
          # added:
          format.js   { render json: @donador.errors, status: :unprocessable_entity }
        end
    end
  end

  #GET /donadores/:id
  def show
  
  end
  
  def edit
    set_donador
    #@donador = Donador.find(params[:id])
  end
  
  def update
    set_donador
    #@donador = Donador.find(params[:id])
    respond_to do |format|
      if set_donador.update_attributes(donador_params)
        flash[:Exito] = "Datos del donador actualizado"
        format.html { redirect_to donadores_path}
        format.json { render :index, status: :ok,location: @donador }
      else
        format.html {render :edit}
        format.json {render json: @donador.errors,status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @donador.destroy
    respond_to do | format |
      flash[:Exito] = "Donador eliminado"
      format.html {redirect_to donadores_url}
      format.json {head :no_content}
    end
  end
  
  private
  
  def set_donador
    @donador = Donador.find(params[:id])
  end
  def donador_params
    params.require(:donador).permit(:nombre, :direccion,:tipo,:telefono, :correo,:RFC,:aniversario)
  end
end
