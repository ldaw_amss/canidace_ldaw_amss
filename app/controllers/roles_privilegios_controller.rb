class RolesPrivilegiosController < ApplicationController
  before_action :validate_user
  
  before_action do
    tiene_permiso_controller(current_user, 6)
  end
  
  before_action only: [:new, :create] do
    tiene_permiso_controller(current_user, 2)
  end
  
  before_action only: [:destroy] do
    tiene_permiso_controller(current_user, 3)
  end
  
  # GET /roles/new
  def new
    @role = Role.find(params[:idRol])
    @privilegios = Privilegio.where.not(idPrivilegio: RolesPrivilegio.where(idRol: @role.idRol).map(&:idPrivilegio)) 
  end
  
  # POST /roles
  # POST /roles.json
  def create
    @role = Role.find(params[:idRol])
    @idPrivilegios = params[:privilegio]
    if(@idPrivilegios.count > 0)
      @idPrivilegios.each do |p| 
        @rolePrivilegio = RolesPrivilegio.new(:idRol => @role.idRol, :idPrivilegio => p)
        if @rolePrivilegio.save
          flash[:Exito] = "Privilegio(s) añadidos!"
        else
          flash[:Error] = "Error al intentar guardar los privilegios. Intentelo más tarde."
        end
      end
    else
      flash[:Error] = "No se han seleccionado privilegios"
    end
    redirect_to @role
  end


  # DELETE /roles/1
  # DELETE /roles/1.json
  def destroy
    #@rolePrivilegio.destroy
    @idRol = params[:idRol]
    @idPrivilegio = params[:idPrivilegio]
    RolesPrivilegio.delete_all(idRol: @idRol, idPrivilegio: @idPrivilegio)
    flash[:Exito] = "Privilegio eliminado"
    redirect_to "/roles/#{@idRol}"
  end
end
