class RolesController < ApplicationController
  before_action :validate_user
  
  before_action do
    tiene_permiso_controller(current_user, 6)
  end
  
  before_action only: [:new, :create] do
    tiene_permiso_controller(current_user, 1)
  end
  
  before_action only: [:edit, :update] do
    tiene_permiso_controller(current_user, 2)
  end
  
  before_action only: [:destroy] do
    tiene_permiso_controller(current_user, 3)
  end

  # GET /roles
  # GET /roles.json
  def index
    @roles = Role.all.paginate(page: params[:page])
  end
  
  # GET /roles/new
  def new
    @role = Role.new
  end
  
  # POST /roles
  # POST /roles.json
  def create
    @role = Role.new(role_params)
    if @role.save
      flash[:Exito] = 'Rol creado.'
      redirect_to @role
    else
      flash[:Error] = "Rol no creado. Inténtelo más tarde"
      @roles = Role.all.paginate(page: params[:page])
      render :index 
    end
  end

  # GET /roles/1
  # GET /roles/1.json
  def show
    @roleX = Role.find_by(idRol: params[:idRol])
    #@roleX.reload
  end

  # GET /roles/1/edit
  def edit
     set_role
  end

  # PATCH/PUT /roles/1
  # PATCH/PUT /roles/1.json
  def update
    if set_role.update_attributes(role_params)
      flash[:Exito] = "Nombre de Rol actualizado"
      redirect_to roles_path 
    else
      render :edit 
    end
  end

  # DELETE /roles/1
  # DELETE /roles/1.json
  def destroy
    set_role
    @usuarios = Usuario.where(idRol: @role.idRol)
    if(@usuarios.count == 0)
      #RolesPrivilegio.where(idRol: @role.idRol)
      RolesPrivilegio.delete_all(idRol: @role.idRol)
      Role.delete_all(idRol: @role.idRol)
      flash[:Exito] = "Rol eliminado"
      redirect_to roles_url
    else
      flash[:Exito] = "No se pudo eliminar el rol. Existen usuarios con ese rol, aigna otro rol a esos usuarios e intentelo de nuevo."
      redirect_to roles_url
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_role
      @role = Role.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def role_params
      params.require(:role).permit(:Nombre)
    end
end
