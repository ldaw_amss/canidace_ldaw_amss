class UsuarioPassController < ApplicationController
  before_action :validate_user
  
  before_action do
    tiene_permiso_controller(current_user, 5)
  end
  
  before_action only: [:edit, :update] do
    tiene_permiso_controller(current_user, 2)
  end
  
  def edit
     @usuario = Usuario.find(params[:id])
     #render :edit
  end
  
  def update
    @usuario = Usuario.find(params[:id])
    if @usuario.update_attributes(usuario_pass_params)
      flash[:Exito] = "Contraseña actualizada"
      redirect_to @usuario
    else
      flash[:Error] = "Contraseña no actualizada. Confirmación no coincide"
      redirect_to @usuario
    end
  end
  
  private
    def usuario_pass_params
      params.require(:usuario).permit(:password, :password_confirmation)
    end
end
