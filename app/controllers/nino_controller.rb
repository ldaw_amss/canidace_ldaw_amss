class NinoController < ApplicationController
    before_action :validate_user
    def show
        @nino = Nino.find(params[:id])
        if (@nino.idTerapeuta != nil)
            @terapeuta = Usuario.find(@nino.idTerapeuta)
        end
    end
    
    def new
        @nino = Nino.new
    end
    
    def create
        @nino = Nino.new(nino_params)
        if @nino.save
            flash[:success] = "registrado exitosamente!"
            redirect_to @nino
        else
            render 'new'
        end
    end
    
    def index
        if(params[:query] != nil)
            require 'will_paginate/array'
            @nino = Nino.nombre_completo(params[:query]).paginate(page: params[:page])
        else
            @nino = Nino.all.order(:nombre).paginate(page: params[:page])
        end
    end
    
    def activate
    @nino = Nino.find(params[:idNino])
    if @nino.update_attribute(:Activo, params[:data])
    else
      
    end
     #redirect_to '/usuarios'
    end
    
    def edit
        @nino = Nino.find(params[:id])
    end
    
    def update
        @nino = Nino.find(params[:id])
        if @nino.update_attributes(nino_params)
            redirect_to @nino
        else
            render 'edit'
        end
    end
    
    
    private

    def nino_params
      params.require(:nino).permit(:nombre, :ApellidoPaterno, :ApellidoMaterno, :FechaNacimiento, :Fechaingreso, :Edad, :Sexo, :Activo,:PorcentajeBeca, :Direccion)
    end
    
end