class Usuario < ApplicationRecord
  #attr_accessor :remember_token
  #before_save :downcase_correo 
  has_many :pagos
  has_many :listas
  has_many :productos, through: :listas
  has_one :padre, foreign_key: "idUsuario",
                   dependent:   :destroy
  #has_one :terapeuta, foreign_key: "idUsuario",
  #                 dependent:   :destroy
  has_many :ninos, foreign_key: "idTerapeuta",
                   dependent:   :destroy
  belongs_to :role, foreign_key: "idRol"
  
  validates :Nombre, presence: true, length: { maximum: 45 }                                      
  validates :ApellidoPaterno, presence: true, length: { maximum: 45 }                 
  validates :ApellidoMaterno, presence: true, length: { maximum: 45 }                 
  validates :FechaNacimiento, presence: true                            
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :Correo, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :Telefono, presence: true, length: { maximum: 50 }                                    
  validates :Sexo, presence: true                           
  #validates :Foto                    
  validates :idRol, presence: true
  
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, on: :create
  
  # Returns the hash digest of the given string.
  def Usuario.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  def Usuario.nombre_completo(query)
    if Rails.env.development?
      db = "skoolapp_development_db" 
    elsif Rails.env.test?
      db = "skoolapp_test_db"
    elsif Rails.env.production?
      db = "skoolapp_production_db"
    end
    find_by_sql("SELECT *, CONCAT(#{db}.usuarios.Nombre, \" \", #{db}.usuarios.ApellidoPaterno,
      \" \", #{db}.usuarios.ApellidoMaterno) as NombreCompleto 
      FROM #{db}.usuarios
      HAVING (NombreCompleto LIKE \"%#{query}%\")
      ORDER BY NombreCompleto asc")
  end
  
  def Usuario.nombre_completo_terapeuta(query)
    if Rails.env.development?
      db = "skoolapp_development_db" 
    elsif Rails.env.test?
      db = "skoolapp_test_db"
    elsif Rails.env.production?
      db = "skoolapp_production_db"
    end
    find_by_sql("SELECT *, CONCAT(#{db}.usuarios.Nombre, \" \", #{db}.usuarios.ApellidoPaterno,
      \" \", #{db}.usuarios.ApellidoMaterno) as NombreCompleto 
      FROM #{db}.usuarios WHERE #{db}.usuarios.idRol = 2
      HAVING (NombreCompleto LIKE \"%#{query}%\") 
      ORDER BY NombreCompleto asc")
  end

  private
    # Converts email to all lower-case.
    def downcase_correo
      Usuario.Correo = Correo.downcase
    end

end