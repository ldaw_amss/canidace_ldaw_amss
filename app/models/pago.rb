class Pago < ApplicationRecord
    belongs_to :usuario
    
    validates :cantidad, {presence: true, length: {minimum: 2}}
    validates :fecha_pago, {presence: true}
    validates :usuario_id, {presence: true}
end
