class Nino < ApplicationRecord
    has_one :padre, foreign_key: "idNino",
                   dependent:   :destroy
    #has_one :terapeuta, foreign_key: "idNino",
    #              dependent:   :destroy
    has_one :usuario, foreign_key: "idUsuario", as: "terapeuta",
                  dependent:   :destroy
    #has_one :usuario, :through => :padre
    has_many :historials, foreign_key: "idNino",
                   dependent:   :destroy
                   
    validates :nombre, presence: true, length: { maximum: 45 }
    validates :ApellidoPaterno, presence: true, length: { maximum: 45 }
    validates :ApellidoMaterno, presence: true, length: { maximum: 45 }
    validates :FechaNacimiento, presence: true
    validates :Edad, presence: true, numericality: true 
    validates :PorcentajeBeca, presence: true
    #validates :Sexo, presence: true
    #validates :Activo, presence: true
    validates :Fechaingreso, presence: true
    validates :PorcentajeBeca, presence: true
    validates :Direccion, presence: true
    
    
    def Nino.nombre_completo(query)
        if Rails.env.development?
            db = "skoolapp_development_db" 
        elsif Rails.env.test?
            db = "skoolapp_test_db"
        elsif Rails.env.production?
            db = "skoolapp_production_db"
        end
        find_by_sql("SELECT *, CONCAT(skoolapp_development_db.ninos.nombre, \" \", skoolapp_development_db.ninos.ApellidoPaterno,
        \" \", skoolapp_development_db.ninos.ApellidoMaterno) as NombreCompleto 
        FROM skoolapp_development_db.ninos
        HAVING (NombreCompleto LIKE \"%#{query}%\")
        ORDER BY NombreCompleto asc")
    end
end