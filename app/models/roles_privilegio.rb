class RolesPrivilegio < ApplicationRecord
    belongs_to :role, foreign_key: "idRol"
    belongs_to :privilegio, foreign_key: "idPrivilegio"
    validates :idPrivilegio, presence: true
    validates :idRol, presence: true
end

