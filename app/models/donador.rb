class Donador < ApplicationRecord
    has_many :donacions
    before_save { correo.downcase! }
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :nombre, {presence: true, length: {maximum: 60}}
    validates :direccion, {presence: true,length: {minimum: 10, maximum: 200} }
    validates :tipo, {presence: true}
    validates :telefono, {presence: true, length: {minimum: 6, maximum: 20} }
    validates :correo, {presence: true, uniqueness: { case_sensitive: false }, length: {minimum: 6,maximum: 70},format: { with: VALID_EMAIL_REGEX }}
    validates :aniversario, {presence: true}
    validates :RFC, {presence: true}
    
    
    def self.search(search)
      if search
        where('nombre LIKE ?', "%#{search}%")
      else
        all
      end
    end
end
