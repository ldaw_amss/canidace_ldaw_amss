class Role < ApplicationRecord
    has_many :usuarios
    has_many :roles_privilegios, foreign_key: "idRol",
                                 dependent:   :destroy
    has_many :privilegios, :through => :roles_privilegios
    validates :Nombre, presence: true, length: { maximum: 45 } 
end