class Lista < ApplicationRecord
    belongs_to :usuario
    belongs_to :producto
    
    validates :cantidad, {presence: true}
    validates :fecha_asignacion, {presence: true}
    validates :usuario_id, {presence: true}
    validates :producto_id, {presence: true}
end
