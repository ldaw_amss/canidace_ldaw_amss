class Producto < ApplicationRecord
    has_many :listas
    has_many :usuarios, through: :listas
    validates :nombre, {presence: true,uniqueness: true,length: {minimum: 3, maximum: 50}}
    #validates :existencia, {presence: true}
    validates :categoria, {presence: true}
    #validates :asignado, {presence: true}
    
    def self.search(search)
      if search
        where('nombre LIKE ?', "%#{search}%")
      else
        all
      end
    end
end
