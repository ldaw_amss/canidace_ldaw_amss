class Historial < ApplicationRecord
    belongs_to :nino, foreign_key: "idNino"
    default_scope -> { order(created_at: :desc) }
    validates :idNino, presence: true
    validates :Diagnostico, length: {maximum: 254}, allow_nil: false
    validates :Medicamentos, length: {maximum: 800}, allow_nil: true
    validates :Medico, length: {maximum: 254}, allow_nil: true
    validates :Pediatra, length: {maximum: 254}, allow_nil: true
    validates :Neurologo, length: {maximum: 254}, allow_nil: true
    validates :Alergias, length: {maximum: 254}, allow_nil: true
    validates :Cirugias, length: {maximum: 254}, allow_nil: true
    validates :Valvulas, length: {maximum: 254}, allow_nil: true
    validates :Fracturas, numericality: true, length: {in: 0..100}, allow_nil: true
    validates :Problemas, length: {maximum: 254}, allow_nil: true
    validates :Terapias, length: {maximum: 254}, allow_nil: true
    validates :Comida, length: {maximum: 254}, allow_nil: true
    validates :Leche, length: {maximum: 254}, allow_nil: true
    validates :Otro, length: {maximum: 254}, allow_nil: true
    validates :Progreso, length: {maximum: 254}, allow_nil: true
end