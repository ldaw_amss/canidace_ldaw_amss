class Terapeuta < ApplicationRecord
    belongs_to :usuario, foreign_key: "idUsuario"
    belongs_to :ninos, foreign_key: "idNino"
    validates :idUsuario, presence: true
    validates :idNino, presence: true
end
