class Privilegio < ApplicationRecord
    has_many :roles_privilegios, foreign_key: "idPrivilegio",
                                 dependent:   :destroy
    has_many :roles, :through => :roles_privilegios
end


