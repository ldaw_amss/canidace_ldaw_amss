class Donacion < ApplicationRecord
    belongs_to :donador
    
    validates :cantidad, {presence: true, length: {minimum: 2}}
    validates :fecha_donacion, {presence: true}
    validates :donador_id, {presence: true}
    
end
