class Padre < ApplicationRecord
    belongs_to :usuario, foreign_key: "idUsuario"
    belongs_to :nino, foreign_key: "idNino"
    validates :idUsuario, presence: true
    validates :idNino, presence: true
end